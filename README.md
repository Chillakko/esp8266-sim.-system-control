# Control System with a Led and a NodeESP8266
For the creation of this software we based ourselves in real world platforms, this is the case to create a user friendly dashboard.
In this programe we used a small amount of outside content, such as:
- XAMPP (for server purposes)
- phpMyAdmin (through XAMPP)
- PanamaHitck Arduino (Node-Java Connection)
- Small amount of HTML/CSS coding

# Full Changelog in Repository
**Ver. 0.1.1(2021-01-20)**
- Full interfaces Change
- Implementation of DB+Node Connection

**Ver. 0.0.7(2021-01-19)**
- Bug fixes

**Ver. 0.0.5(2021-01-18)**
- All dashboard and interfaces have been updated to have a more optimize look
- ESP8266 buttons no longer send to blank websites

**Ver. 0.0.3(2021-01-10)**
- All interfaces have been created
- Buttons and links are fully functional

**Ver. 0.0.01(2021-12-28)**
- MySQL/XAMPP Database created
- Login form, session, logout and MySQL Connection defined and created

# Source and Credit
For purposes of giving the content creators credit, we will credit each on of the contests we used.

1. Java Form creations, buttons, tables, etc with [Netbeans](https://www.youtube.com/channel/UCCstv4vk254i2SJXhDIFxtw)
2. [PanamaHitck_Arduino Library](https://github.com/PanamaHitek/PanamaHitek_Arduino)
3. NodeESP8266 [Connection](https://www.youtube.com/watch?v=YjqCQ0WNgyI)
4. Showing information on tables with [MySQL connection](https://hacksmile.com/rs2xml-jar-free-download/)
