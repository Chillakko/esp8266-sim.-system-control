# Ver. 0.1.1(2021-01-20)
- Interfaces for all pages have been optimized
- Creation of new and functional page that displays hours/status of LED when turned on or off
- Creation and fully functional web page to control NODE via APACHE Server
- Fixed broken pages that diplayed no information

# Ver. 0.0.7(2021-01-19)
**Fixlog**
- Fixed issue where mySQL wouldn't disconnect appropriately(XAMPP)
- Fixed user interface for ADMIN, where loging out would require reset of entire program 
- Fixed Admin help page, set dimensions made tables impossible to visualize correctly

# Ver. 0.0.5(2021-01-18)
As of January 18 2021:
- All dashboard and interfaces have been updated to have a more optimize look
- ESP8266 buttons no longer send to blank websites
- Fixed bug where XAMPP was needed to login-logout
- Added more instrucctions in Arduino/Node screens

# Ver. 0.0.3(2021-01-10)
- All interfaces have been created
- Buttons and links are fully functional
- Database login functional, can no longer access without credentials in DB
- MySQL-Java Connection functional
- Login no longer shows two dashboards for admin users

# Ver. 0.0.01(2021-12-28)
- Admin dashboard created
- MySQL/XAMPP Database created
- Main window and menu items defined
- Login form, session, logout and MySQL Connection defined and created
