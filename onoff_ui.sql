-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2021 at 11:23 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onoff_ui`
--

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `UID` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Nickname` varchar(50) NOT NULL,
  `Usertype` varchar(50) NOT NULL,
  `Date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`UID`, `Username`, `Password`, `Nickname`, `Usertype`, `Date`) VALUES
(0, 'ADMIN', 'root', 'Super_User', 'Admin', '2021-01-10 20:01:46'),
(1, 'Canguro_1', '1234', 'Austin!', 'Usuario', '2021-01-10 20:03:10'),
(3, '1', '1', 'FAST_LOGIN', 'Admin', '2021-01-10 22:33:55'),
(4, 'Alces98', '5546', 'Tyrone', 'Usuario', '2021-01-10 23:03:11'),
(5, 'Uniqua212', '21321434', 'OnlyUniqua1', 'Usuario', '2021-01-10 23:04:41'),
(6, 'Pinguenz0', '00202', 'Pabloo', 'Usuario', '2021-01-10 23:04:41'),
(7, 'Hippoz2', '12353', 'Tashaz', 'Usuario', '2021-01-10 23:05:14'),
(10, '2', '2', 'Fast_User', 'Usuario', '2021-01-20 02:29:45');

-- --------------------------------------------------------

--
-- Table structure for table `statusled`
--

CREATE TABLE `statusled` (
  `ID` int(10) NOT NULL,
  `Stat` varchar(10) NOT NULL,
  `Time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `statusled`
--

INSERT INTO `statusled` (`ID`, `Stat`, `Time`) VALUES
(0, '0', '2021-01-20 06:50:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `statusled`
--
ALTER TABLE `statusled`
  ADD PRIMARY KEY (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
